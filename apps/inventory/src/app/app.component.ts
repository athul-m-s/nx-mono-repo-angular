import { Component } from '@angular/core';
import { NxWelcomeComponent } from './nx-welcome.component';
import { ProductListComponent } from '@angular-monorepo/products';
import { SharedUiComponent } from '@angular-monorepo/shared-ui';

@Component({
  standalone: true,
  // imports: [NxWelcomeComponent],
  imports: [ProductListComponent, SharedUiComponent],
  selector: 'angular-monorepo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'inventory';
}
