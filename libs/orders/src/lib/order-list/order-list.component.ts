import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiComponent } from '@angular-monorepo/shared-ui';

@Component({
  selector: 'angular-monorepo-order-list',
  standalone: true,
  imports: [CommonModule, SharedUiComponent],
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css'],
})
export class OrderListComponent {}
