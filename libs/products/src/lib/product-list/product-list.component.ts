import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiComponent } from '@angular-monorepo/shared-ui';

@Component({
  selector: 'angular-monorepo-product-list',
  standalone: true,
  imports: [CommonModule, SharedUiComponent],
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
})
export class ProductListComponent {}
